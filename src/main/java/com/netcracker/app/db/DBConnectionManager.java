package com.netcracker.app.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class DBConnectionManager {
    private static final String resourceName = "dbconnection.properties";
    private static final String driverName = "org.postgresql.Driver";
    private Connection connection;
    private Properties props;


    public DBConnectionManager() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        props = new Properties();
        InputStream resourceStream = loader.getResourceAsStream(resourceName);
        try {
            props.load(resourceStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            Class.forName(driverName);
            try {
                connection = DriverManager.getConnection(
                        props.getProperty("url"),
                        props.getProperty("user"),
                        props.getProperty("password")
                );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
