package com.netcracker.app;

import com.netcracker.app.db.DBConnectionManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    private static final String SQL_QUERY_GET_STUDENTS = "SELECT * FROM students";

    public static void main(String[] args) {
        DBConnectionManager cm = new DBConnectionManager();
        Connection c = cm.getConnection();
        try {
            Statement statement = c.createStatement();
            ResultSet resultSet =  statement.executeQuery(SQL_QUERY_GET_STUDENTS);
            while(resultSet.next())
                System.out.println(resultSet.getString(1)+' '+resultSet.getString(2));
        } catch(SQLException e){
            e.printStackTrace();
        }

    }
}
